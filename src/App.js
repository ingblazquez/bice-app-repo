import React from "react";
import { Switch, Route } from "react-router-dom";
import Layout from "./components/Layout";
import Dashboard from "./components/Dashboard";

function App() {
 return (
  <Layout>
   <Switch>
    <Route exact path="/" component={Dashboard} />
   </Switch>
  </Layout>
 );
}

export default App;
