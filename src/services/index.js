export const URL_LAST_INDICES = "/last";
export const URL_INDICES_HIST_BY_KEY = "/values/"; // /:key
// "cobre|dolar|euro|ipc|ivp|oro|plata|uf|utm|yen
export const URL_INDICE_BY_DATE = "/date/"; // /:key/:date
