import axios from "axios";
import types from "./types";
import { URL_LAST_INDICES, URL_INDICES_HIST_BY_KEY } from "../../services";

const indicesKeys = [
 "cobre",
 "dolar",
 "euro",
 "ipc",
 "ivp",
 "oro",
 "plata",
 "uf",
 "utm",
 "yen",
];
const fetchLastIndices = () => {
 return async (dispatch) => {
  try {
   const lastIndices = await axios.get(URL_LAST_INDICES);
   const indicesHistRequest = indicesKeys.map((key) =>
    axios.get(URL_INDICES_HIST_BY_KEY + key)
   );
   const indicesHistResult = await Promise.all(indicesHistRequest);

   dispatch(setLastIndices(lastIndices.data));
   dispatch(setIndicesHist(indicesHistResult.map(({ data }) => data)));
  } catch (err) {
   dispatch(setError(err));
  } finally {
   dispatch(setLoading(false));
  }
 };
};

const setLastIndices = (payload) => ({
 type: types.SET_LAST_INDICES,
 payload,
});

const setIndicesHist = (payload) => ({
 type: types.SET_INDICES_HIST,
 payload,
});
const setLoading = (payload) => ({
 type: types.SET_LOADING,
 payload,
});
const setError = (payload) => ({
 type: types.SET_ERROR,
 payload,
});

export default {
 fetchLastIndices,
 setLastIndices,
 setLoading,
 setIndicesHist,
 setError,
};
