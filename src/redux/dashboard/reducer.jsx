import types from "./types";

const INITIAL_STATE = {
 lastIndices: {
  dolar: null,
  euro: null,
  yen: null,
  oro: null,
  plata: null,
  uf: null,
  utm: null,
  cobre: null,
  ipc: null,
  ivp: null,
 },
 indicesHist: [],
 loading: true,
 error: null,
};

const dashboardReducer = (state = INITIAL_STATE, action) => {
 switch (action.type) {
  case types.SET_LAST_INDICES:
   return {
    ...state,
    lastIndices: action.payload,
   };
  case types.SET_LOADING:
   return {
    ...state,
    loading: action.payload,
   };
  case types.SET_INDICES_HIST:
   return setIndicesHist(state, action);
  case types.SET_ERROR:
   return {
    ...state,
    error: action.payload,
   };

  default:
   return state;
 }
};

export default dashboardReducer;

const setIndicesHist = (state, { payload }) => {
 const parsedValues = payload.map((item) => {
  //from objects to array of objects
  const { values } = item;
  const newValues = Object.keys(values).map((item) => ({
   date: Number(item),
   value: values[item],
  }));
  const sortedValues = newValues.sort(function (a, b) {
   return a.date - b.date;
  });
  const dateFormattedValues = sortedValues.map((item) => ({
   ...item,
   date: parseDateES(item.date),
  }));
  return { ...item, values: dateFormattedValues };
 });
 return { ...state, indicesHist: parsedValues };
};

const parseDateES = (unix_timestamp) => {
 let newDay = new Date(unix_timestamp * 1000);
 const day =
  newDay.getDate().toString().length === 1
   ? `0${newDay.getDate()}`
   : newDay.getDate();
 const month =
  (newDay.getMonth() + 1).toString().length === 1
   ? `0${newDay.getMonth() + 1}`
   : newDay.getMonth() + 1;
 return `${day}-${month}-${newDay.getFullYear()}`;
};
