import { combineReducers } from "redux";
import dashboard from "../redux/dashboard/reducer";
const rootReducer = combineReducers({ dashboard });

export default rootReducer;
