import React from "react";
import Grid from "@material-ui/core/Grid";
import logo from "../../../images/logo.png";
import "./header.css";

export default function Header() {
 return (
  <Grid container className="header">
   <Grid item xs={12}>
    <div className="header__logo-container">
     <img className="header__logo" src={logo} alt="bice lab logo" />
    </div>
   </Grid>
   <Grid item xs={12} className="header__title-containter">
    <h1 className="header__title">Indicadores</h1>
   </Grid>
  </Grid>
 );
}
